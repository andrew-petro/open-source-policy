# Guidance for implementing policy

## Where to start

+ Start with new projects, new code, new repositories. It's easier to implement
  open from the beginning.
+ Start with projects where the participants are interested in and comfortable
  with producing open source sofware.

## What license

Where license choice is discretionary choose MIT or APL2.

Adopting another license would require additional approval and justification.
Initiate that with your supervisor.

## Dependencies

Some dependencies are under open source software licenses that impose
constraints on the licenses of software that depend upon those dependencies.
(Example: GPLv3).

Likewise, some dependencies are under proprietary software licenses that forbid
the distribution of those dependencies.

Of course, in all cases we must comply with the licenses of dependencies that we
use.

## Where Git repos are hosted and how they connect

### Example: Direct use of public repositories

Project stereotype: new app (runs in browser, has no direct access to datasources)

Domain: billing (few users, internal data, low-profile disclosure risk)

Dev Team: any

Recommendation: open development (upstream and forks in public git project, anyone can see and submit pull requests)

### Example: Local private repositories selectively mirroring to a public repository

Project stereotype: existing service (runs on server, has access to multiple datasources)

Domain: student data (large scope, restricted data, high-profile disclosure risk)

Dev Team: inexperienced with collaborative development and security practices

Recommendation: mirroring (upstream and forks in private GitLab project restricted to dev team, and public GitHub project mirroring upstream)

### Example: purely private repository

For example, MyUW stores the *private* SSL certificate keys for the MyUW servers
in a git.doit repository. There are good reasons for this in leveraging git.doit
for versioning, as a transport mechanism to the production servers, etc.
Private keys are precisely the sort of private credential contemplated in the
guidance about what *not* to open source, so this repository would remain
private and closed.

## See also

More guidance

+ US Digital Service Digital Services Playbook: [Play 13: Default to open][]

+ [gov.uk Making source code open and re-usable][]
+ [gov.uk Security considerations when coding in the open][]

Examples and case studies

+ [gov.uk Opening a Puppet repository][]
+ [gov.uk Ministry of Justice: why we code in the open][]

[Play 13: Default to open]: https://playbook.cio.gov/#play13

[gov.uk Making source code open and re-usable]: https://www.gov.uk/service-manual/technology/making-source-code-open-and-reusable
[gov.uk Security considerations when coding in the open]: https://www.gov.uk/government/publications/open-source-guidance/security-considerations-when-coding-in-the-open

[gov.uk Opening a Puppet repository]: https://technology.blog.gov.uk/2016/01/19/opening-gov-uks-puppet-repository/
[gov.uk Ministry of Justice: why we code in the open]: https://mojdigital.blog.gov.uk/2017/02/21/why-we-code-in-the-open/
