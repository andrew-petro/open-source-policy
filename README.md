# open-source-policy

(DRAFT) open source policy.

This is a DRAFT of a policy that might be advocated for, grassroots-up, by some
DoIT staff interested in reduced frictions and better support for practicing
open source software development within DoIT.

THIS IS NOT A CURRENTLY ADOPTED POLICY.

## TL;DR

+ [gov.uk has this figured out][gov.uk be open and use open source],
  let's do what they do.
+ default to open, default to MIT or APL2, get help when encountering GPL
+ no worries

## Purpose of this repo

Collaboratively draft a policy suitable to then advocate be adopted in DoIT.

## Purpose of the policy

Clarify university support for DoIT projects generating free and open source
software.

## DRAFT policy

WHEREAS,

producing open source software, open documentation, and open data

+ reduces friction and barriers to understanding one another and our work,
  enabling discovery and pursuit of opportunities to collaborate, learn from one
  another, re-use, and build upon the IT investments of the University
+ increases transparency about the work of the University,
  supporting accountability to the public on whose behalf we work,
  proactively fulfilling the spirit of Wisconsin open records laws and policies
  by making information publicly available even before members of the public
  take the trouble to request specfic documents
+ creates the possibility of collaboration and shared investment across the
  thousands of higher education institutions and beyond
+ creates the possibility that the University might be better served by vendors
  and consultants who can more readily identify opportunities to add value
  through reduced barriers to understanding the IT work of the University,
+ creates the possibility that others in and beyond Wisconsin might learn from,
  benefit from, and build upon DoIT's IT investments, living out the
  Wisconsin Idea that the value created by the University extends beyond the
  bounds of the campus

THEREFORE

all DoIT-produced source code repositories should

+ be publicly visible and clearly licensed under an Open Source licence
  accepted by the Open Source Initiative, OR
+ clearly document why the repository cannot be made publicly visible and take
  care to limit the repository to only content for which the closedness
  justification applies (minimizing incidental closedness).

## Background

### What is open source

Open source software is software the source code of which is made publicly
available under a license that allows the public to use, modify, and
redistribute it.

## Resources

### When code should be opened or closed

gov.uk Government Digital Service
[guidance on "When code should be opened or closed"][]:

You should keep some data and code closed, including:

+ keys and credentials
+ algorithms used to detect fraud
+ unreleased policy

You should open all other code. This includes:

+ configuration code
+ database schema
+ security-enforcing code

[gov.uk be open and use open source]: https://www.gov.uk/guidance/be-open-and-use-open-source
[guidance on "When code should be opened or closed"]: https://www.gov.uk/government/publications/open-source-guidance/when-code-should-be-open-or-closed
